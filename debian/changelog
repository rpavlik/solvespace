solvespace (3.0.rc2+repack1-1) unstable; urgency=medium

  * New upstream version 3.0.rc2+repack1
  * Adjust get_orig.sh script for RC2
  * d/rules: Specify package version for CMake.
  * Drop 20_use_system_flatbuffers: Upstream dep removed.
  * Add 02_add_debian_pkg_version.patch to provide better
    version ID to upstream
  * Add 03_use_system_threejs.patch, update d/control to use
    system-provided three.js
  * Add 04_use_system_unifont.patch, update d/control to use
    system unifont
  * Add 10_mimalloc_restrict_cpu_yield.patch to fix mimalloc
    armel build.
  * Add 11_update_js_three_r111.patch to support three.js r111
    as found in unstable, instead of old r76 bundled in tree.
  * d/control
    - Fix warning about substitution variable
    - Update description to cover new operations
    - Update version in Build-Depends for CMake
    - Remove Build-Depends on libflatbuffers-dev.
  * Add missing_sources for hammer js
  * Update copyright file
  * Add autopkgtests.
  * Package more docs
  * Add manpage for solvespace-cli from notesalexp repo.
  * Update solvespace manpage
  * Ensure build flags are exported and used

 -- Ryan Pavlik <ryan@ryanpavlik.com>  Thu, 21 Jan 2021 11:27:53 -0600

solvespace (3.0.rc1+repack1-4) unstable; urgency=medium

  * [3fa5dd1] Remove arm-patch

 -- Anton Gladky <gladk@debian.org>  Fri, 15 Jan 2021 15:40:02 +0100

solvespace (3.0.rc1+repack1-3) unstable; urgency=medium

  * [fa2ba63] Use CXX for mimalloc compilation

 -- Anton Gladky <gladk@debian.org>  Thu, 14 Jan 2021 23:41:14 +0100

solvespace (3.0.rc1+repack1-2) unstable; urgency=medium

  * [7192813] Update d/copyright
  * [93f958e] Fix compilation on armel

 -- Anton Gladky <gladk@debian.org>  Thu, 14 Jan 2021 19:41:26 +0100

solvespace (3.0.rc1+repack1-1) unstable; urgency=medium

  * [2c974c1] New upstream version 3.0.rc1+repack1
  * [d29ee68] Update get_orig script
  * [73cbc23] Refresh patches
  * [d8e0c6c] Use gtk3. (Closes: #967751)
  * [09453c9] Update watch file format version to 4.
  * [70475f7] Set Standards-Version to 4.5.1

 -- Anton Gladky <gladk@debian.org>  Tue, 12 Jan 2021 23:27:41 +0100

solvespace (2.3+repack1-4) unstable; urgency=medium

  * [f5f8b5c] Add link to the fixed by upstream issue
  * [4ac4b71] Use secure URI in Homepage field.
  * [ef3f5da] Bump debhelper from old 10 to 13.
  * [047d9c4] Set debhelper-compat version in Build-Depends.
  * [8b28a26] Set upstream metadata fields: Bug-Database, Bug-Submit.
  * [fba85d0] Update Vcs-* headers from URL redirect.
  * [ab740f9] Use canonical URL in Vcs-Git.
  * [84a98ae] Replace libglu-dev by libglu1-mesa-dev in BD
  * [fdf3c0d] Set Standards-version to 4.5.0. No changes
  * [cefcb01] Set Rules-Requires-Root: no
  * [9869a0d] Add .gitlab-ci.yml
  * [53ab556] Install usr/share/pixmaps

 -- Anton Gladky <gladk@debian.org>  Mon, 01 Jun 2020 22:17:17 +0200

solvespace (2.3+repack1-3) unstable; urgency=medium

  * [6d84f29] Set minimal cmake version to 3.1.0.
  * [be690c9] Fix FTBFS with glibc 2.25. (Closes: #882167)
  * [6c8697c] Set Standards-Version: 4.1.1
  * [2c3d9bd] Set compat level 10
  * [fb232b5] Set maximal hardening options

 -- Anton Gladky <gladk@debian.org>  Wed, 22 Nov 2017 09:54:12 +0100

solvespace (2.3+repack1-2) unstable; urgency=medium

  * [0d4dc2b] Add missing dependency on libslvs1. (Closes: #856937)

 -- Anton Gladky <gladk@debian.org>  Mon, 06 Mar 2017 20:59:20 +0100

solvespace (2.3+repack1-1) unstable; urgency=medium

  * [a7825d4] Add d/watch.
  * [15facd8] Update get_orig.sh script.
  * [4bcf775] New upstream version 2.3+repack1
  * [5bfcd9a] Refresh patch.

 -- Anton Gladky <gladk@debian.org>  Sat, 31 Dec 2016 09:54:59 +0100

solvespace (2.1+repack2-1) unstable; urgency=medium

  * Initial Release. (Closes: #797997)

 -- Anton Gladky <gladk@debian.org>  Mon, 15 Aug 2016 22:31:44 +0200
