#!/bin/bash

RELEASE_NAME=v3.0.rc2
SUFFIX=+repack1

UPSTREAM="https://github.com/solvespace/solvespace.git"
git clone --depth=1 -b ${RELEASE_NAME} ${UPSTREAM}
(
    cd solvespace
    git submodule init extlib/libdxfrw extlib/mimalloc
    git submodule update
)

#rm -rf ./solvespace
#cp -r ./solvespace_copy ./solvespace
#cp -r ./solvespace ./solvespace_copy


rm -rfv solvespace/extlib/freetype
rm -rfv solvespace/extlib/libpng
rm -rfv solvespace/extlib/zlib
rm -rfv solvespace/extlib/si
rm -rfv solvespace/extlib/angle
rm -rfv solvespace/extlib/cairo
rm -rfv solvespace/extlib/pixman
rm -rfv solvespace/extlib/mimalloc/do*
rm -rfv solvespace/extlib/mimalloc/bin
rm -rfv solvespace/debian
rm -rfv solvespace/.gi*
rm -rfv solvespace/.travis
rm -rfv solvespace/res/fonts/unifont.hex.gz
rm -rfv solvespace/res/threejs/three-r76.js.gz

tar cJf solvespace_${RELEASE_NAME}${SUFFIX}.tar.xz solvespace
